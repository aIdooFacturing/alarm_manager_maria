<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow-x : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	
	var appServerUrl = "http://52.169.201.78:8080/App_Store/index.do?categoryId=4";
	window.addEventListener("message", receiveMessage, false);
	
	function receiveMessage(evet){
		var msg = event.data;
		$("#app_store_iframe").animate({
			"left" : - $("#app_store_iframe").width() * 1.5 
		}, function(){
			$("#app_store_iframe").css({
				"left" : originWidth,
				"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
			})
			
			addAppFlag = true;
			$(".addApp").rotate({
				duration:500,
			    angle: 45,
			   	animateTo:0
			});
		});
		
		fileDown(msg.url, msg.appName, msg.appId)
	}
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function fileDown(appUrl, appName, appId){
		var url = "${ctxPath}/fileDown.do";
		var param = "fileLocation=" + appUrl + 
					"&fileName=" + appName + 
					"&appId=" + appId + 
					"&ty=" + categoryId + 
					"&shopId=" + shopId;

		getProgress();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				if(data=="success"){
					getAppList()					
				}
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	var progressLoop = false;
	function getProgress(){
		var url = "${ctxPath}/getProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data!=100){
					$(".progressBar").remove();
					//draw ProgressBar();
					var targetBar = $(".nav_span").next("img").width();
					var currentBar = targetBar * Number(data).toFixed(1)/100;
					
					
					var barHeight = getElSize(50);
					var _top = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().top + $(".nav_span:nth(" + appCnt + ")").parent("td").height() - barHeight;
					var _left = $(".nav_span:nth(" + appCnt + ")").parent("td").offset().left;
					
					var bar = document.createElement("div");
					bar.setAttribute("class", "progressBar");
					bar.style.cssText = "width : " + currentBar + "px;" + 
										"height : " + barHeight + "px;" +
										"position : absolute;" + 
										"top : " + _top + "px;" + 
										"left : " + _left + "px;" +
										"z-index : 99999;" + 
										"background-color : lightgreen;";
										
										
					$("body").prepend(bar);
					
					clearInterval(progressLoop);
					progressLoop = setTimeout(getProgress,500)
					
					$(".nav_span:nth(" + appCnt + ")").html(Number(data).toFixed(1) + "%")	 
					
				}else{
					resetProgress();
				}
				
				
				$(".addApp").remove();
			}
		});
	};
	
	function resetProgress(){
		var url = "${ctxPath}/resetProgress.do";
		
		$.ajax({
			url : url,
			type : "post",
			success : function(data){
			}
		});
	};
	
	function showMinusBtn(parentTd, appId){
		$("#delDiv div:nth(0)").unbind("click");
		
		$(".minus").animate({
			"width" : 0
		}, function(){
			$(this).remove();
		});
		
		var img = document.createElement("img");
		img.setAttribute("src", "${ctxPath}/images/minus.png");
		img.setAttribute("class", "minus");
		img.style.cssText = "position : absolute;" +
							"cursor : pointer;" + 
							"z-index: 9999;" + 
							"left : " + (parentTd.offset().left + parentTd.width()) + "px;" +
							"top : " + (parentTd.offset().top) + "px;" +
							"width : " + getElSize(0) + "px;";
		
		$(img).click(function(){
			$("#delDiv").animate({
				"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
				"top" : (window.innerHeight/2) - ($("#delDiv").height()/2) + getElSize(100)
			}, function(){
				$("#delDiv").animate({
					"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
					"top" : (window.innerHeight/2) - ($("#delDiv").height()/2)
				}, 100)
			});
			
			$("#delDiv div:nth(0)").click(function(){
				removeApp(appId);
			})
		});
		
		$(img).animate({
			"width" : getElSize(80)
		});		
		
		$("body").prepend(img)					
	};
	
	function removeApp(appId){
		var url = "${ctxPath}/removeApp.do";
		var param = "appId=" + appId + 
					"&shopId=" + shopId;
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					$("#delDiv").animate({
						"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
						"top" : - $("#delDiv").height() * 2
					})	
					
					$(".minus").animate({
						"width" : 0
					}, function(){
						$(this).remove();
					});
					getAppList();
					
					console.log("call fucntion remove")
				}
			}
		});
	};
	
	var appArray = [];
	var appCnt = 0;
	function getAppList(){
		resetProgress();
		clearInterval(progressLoop);
		$(".progressBar").remove();
		
		var url = "${ctxPath}/getAppList.do";
		var param = "categoryId=" + categoryId + 
					"&shopId=" +shopId;
		
		appArray = [];
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
		
				$(".nav_span").html("");
				$(".addApp").remove();
				
				//id, appId, name
				
				appCnt = json.length;
				
				$(".nav_span").next("img").unbind("contextmenu").attr("src", "${ctxPath}/images/unselected.png");
				$(".nav_span").unbind("contextmenu");
				
				$(json).each(function(idx, data){
					var appName;
					if(data.name=="Alarm_Manager"){
						appName = "${alarm_manage}"
					}else if(data.name=="Device_Status"){
						appName = "${machine_list}"
					}else if(data.name=="DashBoard_BK"){
						appName = "${dashboard_bk}";
					}else if(data.name=="DashBoard2_BK"){
						appName = "${dashboard_bk2}";
					}else if(data.name=="Single_Chart_Status"){
						appName = "${dailydevicestatus}";
					}else if(data.name=="Production_Board"){
						appName = "${prdct_board}";
					}else if(data.name=="24hChart"){
						appName = "${barchart}";
					}else if(data.name=="DMM"){
						appName = "${dmm}";
					}
					
					appArray.push(data.appId)
					$(".nav_span:nth(" + idx + ")").attr("appId", data.appId);
					$(".nav_span:nth(" + idx + ")").html(appName);
					$(".nav_span:nth(" + idx + ")").click(function(){
						//location.href = "/" + data.name + "/index.do";
						location.href = "http://" + data.url + "?lang=" + window.localStorage.getItem("lang");
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").click(function(){
						/* if(data.name=="DashBoard"){
							location.href = "/DIMF/chart/main.do?categoryId=1";	
						}else{
							location.href = "/" + data.name + "/index.do";	
						} */
						
						location.href = "http://" + data.url + "?lang=" + window.localStorage.getItem("lang");
					});
					
					$(".nav_span:nth(" + idx + ")").next("img").attr("appId", data.appId)
					
					if(data.appId==appId){
						$(".nav_span:nth(" + idx + ")").next("img").attr("src", "${ctxPath}/images/selected_purple.png")
						$(".nav_span:nth(" + idx + ")").css("color", "white");
					}
					$(".nav_span:nth(" + idx + ")").next("img").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).prev("span").attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td");
						showMinusBtn(parentTd, appId)						
					});
					
					$(".nav_span:nth(" + idx + ")").contextmenu(function(e){
						e.preventDefault();
						var appId = $(this).attr("appId");
						var parentTd = $(".nav_span:nth(" + idx + ")").parent("Td")
						showMinusBtn(parentTd, appId)						
					});
				});
				
				var totalCell = 10;
				
				var parentTd = $(".nav_span:nth(" + json.length + ")").parent("td");
				var img = document.createElement("img");
				img.setAttribute("src", "${ctxPath}/images/add.png");
				img.setAttribute("class", "addApp");
				img.style.cssText = "position : absolute;" +
									"cursor : pointer;" + 
									"z-index: 9999;" + 
									"left : " + (parentTd.offset().left + (parentTd.width()/2) - getElSize(40)) + "px;" +
									"top : " + (parentTd.offset().top + (parentTd.height()/2) - getElSize(40)) + "px;" +
									"width : " + getElSize(80) + "px;"; 
									
					
				//$("body").prepend(img);
				
				//iframe 호출
				$(img).click(function(){
					document.querySelector("#app_store_iframe").contentWindow.postMessage(appArray, '*');
					
					if(addAppFlag){
						$(this).rotate({
					   		duration:500,
					      	angle: 0,
					      	animateTo:45
						});
						$("#app_store_iframe").animate({
							"left" : (originWidth/2) - ($("#app_store_iframe").width()/2)
						});	
					}else{
						$(this).rotate({
							duration:500,
						    angle: 45,
						   	animateTo:0
						});
						$("#app_store_iframe").animate({
							"left" : - $("#app_store_iframe").width() * 1.5 
						}, function(){
							$("#app_store_iframe").css({
								"left" : originWidth,
								"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
							})
						});
					}
					
					addAppFlag = !addAppFlag;
				})
			}
		});
	};
	
	var addAppFlag = true;
	var categoryId = 4;
	var appId = 33;
	
	$(function(){
		//getAppList();
		setDate();
		getGroup();
		createNav("mainten_nav", 0);
		
		setEl();
		time();
		
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			//"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
			"position" : "relative",
			"top" : getElSize(26)
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#app_store_iframe").css({
			"width" : getElSize(3100) + "px",
			"height" : getElSize(2000) + "px",
			"position" : "absolute",
			"z-index" : 9999,
			//"display" : "block"
		});
		
		$("#app_store_iframe").css({
			"left" : originWidth * 1.5,
			"top" : (originHeight/2) - ($("#app_store_iframe").height()/2) 
		}).attr("src", appServerUrl)
		
		
		$("#delDiv").css({
			"color" : "white",
			"z-index" : 9999999,
			"background-color" : "black",
			"position" : "absolute",
			"width" : getElSize(700) + "px",
			"font-size" : getElSize(60) + "px",
			"text-align" : "center",
			"padding" : getElSize(30) + "px",
			"border" : getElSize(7) + "px solid rgb(34,34,34)",
			"border-radius" : getElSize(50) + "px"
		});
		
		
		$("#delDiv div").css({
			"background-color" : "rgb(34,34,34)",
			"padding" : getElSize(20) + "px",
			"margin" : getElSize(10) + "px",
			"cursor" : "pointer"
		}).hover(function(){
			$(this).css({
				"background-color" : "white",
				"color" : "rgb(34,34,34)",
			})
		}, function(){
			$(this).css({
				"background-color" : "rgb(34,34,34)",
				"color" : "white",
			})
		});
		
		$("#delDiv").css({
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"top" : - $("#delDiv").height() * 2
		});
		
		$("#delDiv div:nth(1)").click(function(){
			$("#delDiv").animate({
				"top" : - $("#delDiv").height() * 2
			});
		});
		
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		};
		
		// jane 추가
		$("#date_table").css({
			"position" : "absolute",
			"top" : marginHeight + getElSize(225),
			"z-index" : 999,
			"left" : marginWidth + getElSize(600),
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getGroup(){
		var url = "${ctxPath}/getJigList4Report.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dvcList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
						
					option += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";
					//option += "<option value='" + data.dvcId + "'>M" + decodeURIComponent(data.dvcId).replace(/\+/gi," ") + "</option>";
 
				});
				
				$("#group").html(option);
				
				getAlarmData();
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	
	function getAlarmData(){
		var url = "${ctxPath}/getAlarmData.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val() + " 23:59:59";
		
		var param = "sDate=" + sDate +
					"&eDate=" + eDate + 
					"&shopId=" + shopId + 
					"&group=" + $("#group").val();
		
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data)
			var json = data.alarmList;
				
				var tr = "<thead><Tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" + 
							"<td>${device}</td>" + 
							"<td>${alarm}<br>${start_time}</td>" + 
							"<td>${alarm}<br>${end_time}</td>" + 
							"<td>${fixed_time}</td>" + 
							"<td style='width: 50%'>${alarm}${content}</td>" + 
						"</tr></thead><tbody>";
						
				csvOutput = "${device},${alarm}${start_time}, ${alarm}${end_time}, ${fixed_time}, ${alarm}${content}LINE";
				
				$(json).each(function(idx, data){
					
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					
					classFlag = !classFlag;
					
					var endDateTime;
					
					if(data.endDateTime!=null){
						endDateTime = data.endDateTime.substr(0,19);
					};
					
					var delayTimeSec = data.delayTimeSec;
					
					console.log("delayTimeSec : " + delayTimeSec);
					
					if((data.endDateTime == null || data.endDateTime=="") && (data.delayTimeSec == null || data.delayTimeSec=="")){
						endDateTime = "-";
						delayTimeSec = "처리 중";
					}else{
						delayTimeSec = Math.ceil(delayTimeSec/60)
					}
					
					var alarm;
					
					if(data.ncAlarmNum1!=""){
							alarm =  data.ncAlarmNum1 + "-" + decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ");
					};
					
					if(data.ncAlarmNum2!=""){
						alarm = data.ncAlarmNum2 + "-" + decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " "); 
					};
					
					if(data.ncAlarmNum3!=""){
						alarm = data.ncAlarmNum3 + "-" + decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " "); 
					};
					
					
					tr += "<tr class='contentTr " + className + "'>" +
								"<td>" + decode(data.name) + "</td>" +
								"<td>" + data.startDateTime.substr(0,19) + "</td>" + 
								"<td>" + endDateTime + "</td>" + 
								"<td>" + delayTimeSec + "</td>" + 
								"<td>" + decodeURIComponent(alarm) + "</td>";
						"</tr>";
					
					csvOutput += data.name + "," + 
								data.startDateTime.substr(0,19) + "," + 
								endDateTime + "," + 
								Math.ceil(delayTimeSec/60) + "," + 
								decodeURIComponent(alarm) + "LINE";
				});
				
				tr += "</tbody>";
				
				$(".alarmTable").html(tr).css({
					"font-size": getElSize(40),
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"overflow" : "hidden",
					"margin-top" : getElSize(27)
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1400));
				$("#wrapper div:last").css("overflow", "auto")
			}
		});
	};

</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<spring:message  code="chk_del"></spring:message>
		<div><spring:message  code="check"></spring:message></div>
		<div><spring:message  code="cancel"></spring:message></div>
	</div>
	
	<iframe id="app_store_iframe"  style="display: none"></iframe>	
	
	<div id="time"></div>
	<div id="title"><spring:message code="alarm_manage"></spring:message></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
			<!-- jane 추가 --> 
			<div id="date_table">
				<spring:message code="device"></spring:message>
				<select id="group"></select>
				<spring:message code="op_period"></spring:message>
				<input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate">
				<%-- <button> <spring:message code="excel"></spring:message></button> --%>
				<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getAlarmData()">
			</div>
		
		
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<!-- jane 변경 -->
					<%-- <table id="contentTable">
						<Tr>
							<td>
								<spring:message code="device"></spring:message>
								<select id="group"></select>
								<spring:message code="op_period"></spring:message>
								<input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate">
								<button> <spring:message code="excel"></spring:message></button>	// 주
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getAlarmData()">
							</td>
						</Tr>
					</table> --%>
					<div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1">
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	